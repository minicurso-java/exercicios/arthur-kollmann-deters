import java.util.Scanner;
public class aula6exercicio4 {
    public static void main(String[] args) {



                Scanner sc = new Scanner(System.in);

                System.out.printf("Digite o tamanho do vetor: \n");
                int t = sc.nextInt();

                double[] vetor = new double[t];

                System.out.printf("Digite os números reais: \n");
                for (int i = 0; i < t; i++) {
                    vetor[i] = sc.nextDouble();
                }

                double maior = vetor[0];
                int posicaoMaior = 0;

                for (int i = 1; i < t; i++) {
                    if (vetor[i] > maior) {
                        maior = vetor[i];
                        posicaoMaior = i;
                    }
                }

                System.out.printf("O maior número é: %.2f \n", maior);
                System.out.printf("Posição do maior elemento: %d \n", posicaoMaior);

                sc.close();
            }
        }