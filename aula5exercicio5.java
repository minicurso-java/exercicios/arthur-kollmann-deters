import java.util.Scanner;
public class aula5exercicio5 {
     public static void main(String[] args) {
         Scanner sc = new Scanner(System.in);
         char repetir;

         do {
             System.out.printf("Digite a temperatura em Celsius: \n" );
             double celsius = sc.nextDouble();

             double fahrenheit =Fahrenheit(celsius);

             System.out.printf("A temperatura em Fahrenheit é:%.2f ", fahrenheit);

             System.out.println("Deseja repetir o programa? (s/n)");
             repetir = sc.next().charAt(0);
         } while (repetir == 's' || repetir == 'S');

         sc.close();
     }

    public static double Fahrenheit(double celsius) {
        return (celsius * 9 / 5) + 32;
    }
}
     

