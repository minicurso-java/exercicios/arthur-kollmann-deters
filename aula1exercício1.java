package abc;
import java.util.Locale;
import java.util.Scanner;

public class abcdefg {

	public static void main(String[] args) {
		// TODO Auto-generated method stub3
		Scanner sc = new Scanner(System.in);
		Locale.setDefault(Locale.US);	
		
		double x, y, z, tri, circulo, trapezio, quadrado, ret;
		
		System.out.printf("digite os 3 valores");
		x = sc.nextDouble();
		y = sc.nextDouble();
		z = sc.nextDouble();
		
		tri = x * z / 2;
		circulo = 3.14159 * Math.pow(z, 2);
		trapezio = y+x * z / 2;
		quadrado = y*y;
		ret = x*y;
		
		System.out.printf("a área do triângulo é: %.2f \n", tri);
		System.out.printf("a área do circulo é:%.2f \n", circulo);
		System.out.printf("a área do trapézio é:%.2f \n", trapezio);
		System.out.printf("a área do quadrado é:%.2f \n", quadrado);
		System.out.printf("a área do retângulo é:%.2f \n", ret);
	
	
	sc.close();
	}

}
