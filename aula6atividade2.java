import java.util.Scanner;
public class aula6atividade2    {
     public static void main(String[] args) {
         int[] vetor = new int[8];

         Scanner sc = new Scanner(System.in);

         System.out.printf("preencha o vetor \n");

            for(int i = 0; i < vetor.length; i++){
                System.out.printf("digite um número:");
                vetor [i] = sc.nextInt();
            }

         System.out.printf("Digite dois índices para trocar os elementos de posição: \n");
         int i1 = sc.nextInt();
         int i2 = sc.nextInt();

         int aux = vetor[i1];
         vetor[i1] = vetor[i2];
         vetor[i2] = aux;

         System.out.println("Vetor resultante, com os índices trocados: \n");
         for (int i = 0; i < vetor.length; i++) {
             System.out.printf("vetor = %d \n", vetor[i]);
         }
     }
}
